import React from 'react';
import { withRouter } from 'react-router-dom'; // HOC to load navigation history props
import classess from './Burger.css';
import Burgeringredients from './BurgerIngredients/BurgerIngredients';
const burger = ( props ) => {
    let transformedIngredients = Object.keys(props.ingredients).map(igKey => {
        return [...Array(props.ingredients[igKey])].map((_, i) => {
            return <Burgeringredients key={igKey + i} type={igKey}/>;
        });
    })
    .reduce((arr, el) => {
        return arr.concat(el);
    },[]);

    if (transformedIngredients.length === 0) {
        transformedIngredients = <p>Please start adding ingredients</p>;
    }
    return (
        <div className={classess.Burger}>
            <Burgeringredients type="bread-top"/>
            {transformedIngredients}
            <Burgeringredients type="bread-bottom"/>
        </div>
    );

}

export default withRouter(burger);