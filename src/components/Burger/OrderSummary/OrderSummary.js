import React, { Component } from 'react';
import Aux from '../../../hoc/Aux/aux';
import Button from '../../UI/Button/Button';

class OrderSummary extends Component {
    // componentDidUpdate(){
        
    // }
    render() {
        const ingredients = Object.keys(this.props.ingredients).map(igKey => {
            return (<li key={igKey}><span style={{textTransform : "capitalize"}}>{igKey} </span> : {this.props.ingredients[igKey]}</li>);
            });
        return (
            <Aux>
                <h3>Your Order</h3>
                <p>A delicious burger with following ingredients:</p>
                <ul>
                    {ingredients}
                </ul>
                <p style={{ textAlign : 'center', fontWeight : 'bold' }}>Total Price : {this.props.totalPrice}</p>
                <p>Continue to checkout ?</p>
                <Button btnType="Danger" clicked={this.props.modalClosed}>Cancel</Button>
                <Button btnType="Success" clicked={this.props.continueClicked}>Continue</Button>
            </Aux>
        )
    }
}

export default OrderSummary;