import React from 'react';
import classes from './BuildControls.css';
import BuildControl from './BuildControl/BuildControl';

const controls = [
    {label : 'Salad', type: 'salad'},
    {label : 'Bacon', type: 'bacon'},
    {label : 'Cheese', type: 'cheese'},
    {label : 'Meat', type: 'meat'}
];

const buildControls = ( props ) => (
    <div className={classes.BuildControls}>
        <p>Current Price : <strong>{props.totalPrice}</strong> </p>
        {
            controls.map(ctrl => {
                return <BuildControl 
                key={ctrl.label} 
                label={ctrl.label} 
                addIngredient={() => props.ingredientAdded(ctrl.type)}
                lessIngredient = {() => props.ingredientLess(ctrl.type)}
                disabledInfo = {props.disbaled[ctrl.type]}
                
                />
            })
        }

        <button 
            className={classes.OrderButton} 
            disabled = {!props.purchasable}
            onClick={props.ordered}
    >{props.isAuth ? 'ORDER NOW' : 'SIGNUP FOR ORDER'}</button>

    </div>
)

export default buildControls;