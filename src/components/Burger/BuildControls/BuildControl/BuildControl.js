import React from 'react';
import classes from './BuildControl.css';

const buildControl = ( props ) => {
    return (
        <div className={classes.BuildControl}>
        <div className={classes.Lable}>{props.label}</div>
        <button className={classes.Less} onClick={props.lessIngredient} disabled={props.disabledInfo}>Less</button>
        <button onClick={props.addIngredient} className={classes.More}>More</button>

        </div>
    );
};

export default buildControl;