import React from 'react';
import NavigationItem from './NavigationItem/NavigationItem';
import classes from './NavigationItems.css'

const navigationItems = ( props ) => (
    <ul className={classes.NavigationItems}>
       <NavigationItem link="/">Burger Builder</NavigationItem>
       {props.isAutheticated ? <NavigationItem link="/orders">My Orders</NavigationItem> : null}
       {!props.isAutheticated ? <NavigationItem link="/auth">Authentication</NavigationItem> : <NavigationItem link="/logout">LogOut</NavigationItem>}
       
    </ul>
);

export default navigationItems;