import React from 'react';
import classes from './Order.css';

const order = ( props ) => {
    const ingredientsData = [];
    for (let key in props.ingredients) {
        if (props.ingredients[key] > 0) {
            ingredientsData.push({
                name : key,
                amount : props.ingredients[key]
            });
        }
    }

    const ingredientsOutput = ingredientsData.map(ing => {
        return <span 
            style={{textTransform : 'capitalize', display : 'inline-block', margin : '0px 8px', border : '1px solid #ccc', padding : '5px'}}
            key={ing.name}>{ing.name} ({ing.amount})</span>;
    });
    return(
        <div className={classes.Order}>
            <p>Ingredients : {ingredientsOutput}</p>
    <p>Price : <strong>INR {props.price}</strong></p>
        </div>
    )
};

export default order;