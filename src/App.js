import React, { Component } from 'react';
import {connect} from 'react-redux';
import asyncComponent from './hoc/asyncComponent/asyncComponent';

import Layout from './containers/Layout/Layout';
import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import Logout from './containers/Auth/Logout/Logout';
import * as actions from './store/actions/index';

const asyncCheckout = asyncComponent(() => {
  return import('./containers/Checkout/Checkout');
});

const asyncOrder = asyncComponent(() => {
  return import('./containers/Orders/Orders');
});

const asyncAuth = asyncComponent(() => {
  return import('./containers/Auth/Auth');
});

class App extends Component {
  componentDidMount()
  {
    this.props.onCheckAuth();
  }

  render() {
    let guardRoute = (
      <Switch>
          <Route exact path="/" component={BurgerBuilder}/>
          <Route exact path="/auth" component={asyncAuth}/>
          <Redirect to="/" />
      </Switch>
    );
    if (this.props.isAuthenticated) {
      guardRoute = (
        <Switch>
        <Route exact path="/orders" component={asyncOrder}/>
        <Route exact path="/logout" component={Logout} />
        <Route path="/checkout" component={asyncCheckout}/>
        <Route exact path="/auth" component={asyncAuth}/>
        <Route exact path="/" component={BurgerBuilder}/> 
        <Redirect to="/" />
        </Switch>
      );
    }
    return (
      <div>
         <Layout>
           {guardRoute}
         </Layout>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated : state.auth.token !== null
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onCheckAuth : () => dispatch(actions.checkAuthState())
  }
}

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(App));
