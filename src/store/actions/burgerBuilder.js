import * as actionTypes from './actionTypes';
import axios from '../../axios';


export const addIngredient = (name) => {
    return {
        type : actionTypes.ADD_INGREDEIENT,
        ingredientName : name
    }
}

export const removeIngredient = (name) => {
    return {
        type : actionTypes.REMOVE_INGREDEIENT,
        ingredientName : name
    }
}

export const setIngredients = (ingredient) => {
    return {
        type : actionTypes.LIST_INGREDIENTS,
        ingredients : ingredient
    }
}

export const fetchIngredientFailed = () => {
    return {
        type : actionTypes.FETCH_INGREDIENTS_FAILED,
    }
}

export const initIngredients = () => {
    return dispatch => {
        axios.get('/ingredients.json')
            .then(response => {
                dispatch(setIngredients(response.data));
            }).catch(error => {
                dispatch(fetchIngredientFailed);
            });
    }
}