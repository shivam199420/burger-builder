import * as actionTypes from './actionTypes';
import axios from '../../axios';


export const purchaseBurgerSucces = (id, orderData) => {
    return {
        type : actionTypes.PURCHASE_BURGER_SUCCESS,
        id : id,
        orderData : orderData
    }
}

export const purchaseBurgerFail = (error) => {
    return {
        type : actionTypes.PURCHASE_BURGER_FAIL,
        error : error
    }
}

export const purchaseBurgerStart = () => {
    return {
        type : actionTypes.PURCHASE_BURGER_START,
    };
};

export const purchaseInit = () => {
    return {
        type : actionTypes.PURCHASE_INIT
    };
};

export const purchaseBurger = (orderData, token) => {
    return dispatch => {
        purchaseBurgerStart()
        axios.post('/orders.json?auth=' + token,orderData)
        .then(response => {
            dispatch(purchaseBurgerSucces(response.data.name, orderData));
        }).catch(error => {
            dispatch(purchaseBurgerFail(error));
        })
    }
}

export const fetchOrderSuccess = ( order ) => {
    return {
        type : actionTypes.FETCH_ORDER_SUCCESS,
        orders : order
    }
}

export const fetchOrderError = ( error ) => {
    return {
        type : actionTypes.FETCH_ORDER_FAIL,
        error : error
    }
}

export const fetchOrderStart = () => {
    return {
        type : actionTypes.FETCH_ORDER_START
    }
}

export const fetchOrders = (token, userId) => {
    return dispatch => {
        dispatch(fetchOrderStart());
        axios.get('/orders.json?auth=' + token+'&orderBy="userId"&equalTo="' + userId + '"')
        .then(response => {
            let fetchedOrder = [];
            for (let key in response.data) {
                fetchedOrder.push({
                    ...response.data[key],
                    id : key
                });
            }
            dispatch(fetchOrderSuccess(fetchedOrder));
            // this.setState({order : fetchedOrder, orderLoading: false});
        }).catch(err => {
            dispatch(fetchOrderError(err));
        })
    }
}