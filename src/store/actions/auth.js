import * as actionTypes from './actionTypes';
import axios from 'axios';

export const authStart = () => {
    return {
        type : actionTypes.AUTH_START
    }
}

export const authSuccess = ( token,userId ) => {
    return {
        type : actionTypes.AUTH_SUCCESS,
        token : token,
        userId : userId
    }
}

export const authFail = ( error ) => {
    return {
        type : actionTypes.AUTH_FAIL,
        error : error
    }
}

export const authLogout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('userId'); 
    return {
        type : actionTypes.AUTH_LOGOUT
    }
}

export const checkAuthTimeout = (expirationTime) => {
    return dispatch => {
        setTimeout(() => {
            dispatch(authLogout());
        }, expirationTime * 1000)
    }
}

export const authentication = (email, password, isSignUp) => {
    return dispatch => {
        dispatch(authStart());
        const authData = {
            email : email,
            password : password,
            returnSecureToken : true
        };
        let url = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyBvlY0Dv07CTe0MO9Gp1g-qSraja7wU9Hc';
        if (!isSignUp) {
             url = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBvlY0Dv07CTe0MO9Gp1g-qSraja7wU9Hc';
        }
        axios.post(url,authData)
            .then(response => {
                const expirationDate = new Date(new Date().getTime() + response.data.expiresIn * 1000);
                localStorage.setItem('token',response.data.idToken);
                localStorage.setItem('expirationDate',expirationDate);
                localStorage.setItem('userId',response.data.localId); 
                dispatch(authSuccess(response.data.idToken, response.data.localId));
                dispatch(checkAuthTimeout(response.data.expiresIn))
            }).catch(err => {
                dispatch(authFail(err.response.data.error));
            })
    }
}

export const authRedirect = (path) => {
    return {
        type : actionTypes.AUTH_USER_REDIRECT,
        path : path
    }
}

export const checkAuthState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        const userId = localStorage.getItem('userId');
        if (!token) {
            dispatch(authLogout());
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if (expirationDate <= new Date()) {
                dispatch(authLogout());
                dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime())/ 1000))
            } else {
                dispatch(authSuccess(token,userId));
            }
        }
    }
}