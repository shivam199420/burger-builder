import * as actionTypes from '../actions/actionTypes';

import { updatedObject } from '../../shared/utility';
const initialState = {
    token : null,
    userId : null,
    error : null,
    loading : false,
    authRedirect : '/'
}
const reducers = (state = initialState, action) => {
    if (action.type === actionTypes.AUTH_START) {
        return updatedObject(state, {loading : true, error : null})
    }

    if (action.type === actionTypes.AUTH_FAIL) {
        return updatedObject(state, {
            loading : false, 
            token : null,
            userId : null,
            error : action.error
        })
    }

    if (action.type === actionTypes.AUTH_SUCCESS) {
        return updatedObject(state, {
            token : action.token,
            userId : action.userId,
            error : null,
            loading : false
        })
    }

    if (action.type === actionTypes.AUTH_LOGOUT) {
        return updatedObject(state, {
            token : null,
            userId : null,
            error : null,
            loading : false
        })
    }

    if (action.type === actionTypes.AUTH_USER_REDIRECT) {
        return updatedObject(state,{authRedirect : action.path})
    }
    return state;
}

export default reducers;
