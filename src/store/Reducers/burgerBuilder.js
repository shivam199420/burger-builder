import * as actionTypes from '../actions/actionTypes';
import { updatedObject } from '../../shared/utility';

const initialState = {
    ingredients : null,
    totalPrice : 4,
    error : false,
    building : false
};

const ingredients_price = {
    cheese : 2,
    meat : 3.5,
    bacon : 1,
    salad : 1.5
};

const reducer = (state = initialState, action) => {
    if (action.type === actionTypes.ADD_INGREDEIENT) {
        const newIngredients = {[action.ingredientName] : state.ingredients[action.ingredientName] + 1};
        const updatedIngredients = updatedObject(state.ingredients,newIngredients);
        const updatedState = {
            ingredients : updatedIngredients,
            totalPrice : state.totalPrice + ingredients_price[action.ingredientName],
            building : true
        };
        return updatedObject(state,updatedState);
    }

    if (action.type === actionTypes.REMOVE_INGREDEIENT) {
        const newIngredients = {[action.ingredientName] : state.ingredients[action.ingredientName] - 1};
        const updatedIngredients = updatedObject(state.ingredients,newIngredients);
        const updatedState = {
            ingredients : updatedIngredients,
            totalPrice : state.totalPrice - ingredients_price[action.ingredientName],
            building : true
        };
        return updatedObject(state,updatedState);
    }

    if (action.type === actionTypes.LIST_INGREDIENTS) {
        const updatedState = {
            ingredients : action.ingredients,
            error : false,
            totalPrice : 4,
            building : false
        };
        return updatedObject(state,updatedState);
    }

    if (action.type === actionTypes.FETCH_INGREDIENTS_FAILED) {
        return updatedObject(state,{error : true});
    }
    return state;
};

export default reducer;