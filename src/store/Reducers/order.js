import * as actionTypes from '../actions/actionTypes';
import { updatedObject }  from '../../shared/utility';

const initialState = {
   orders : [],
   loading : false,
   purchased : false,
};

const reducer = (state = initialState, action) => {

    if (action.type === actionTypes.PURCHASE_BURGER_START) {
        return updatedObject(state,{loading : true});
    }

    if (action.type === actionTypes.PURCHASE_BURGER_SUCCESS) {
        const newOrder = {
            ...action.orderData,
            id : action.orderId
        }

        const updatedState = {
            loading : false,
            orders : state.orders.concat(newOrder),
            purchased : true
        };
        return updatedObject(state,updatedState); 
    }
    if (action.type === actionTypes.PURCHASE_BURGER_FAIL) {
        return updatedObject(state,{loading : false});
    }

    if (action.type === actionTypes.PURCHASE_INIT) {
        return updatedObject(state,{purchased : false});
    }

    if (action.type === actionTypes.FETCH_ORDER_START) {
        return updatedObject(state,{loading : true});
    }

    if (action.type === actionTypes.FETCH_ORDER_SUCCESS) {
        return updatedObject(state,{loading : false,orders : action.orders});
    }

    if (action.type === actionTypes.FETCH_ORDER_FAIL) {
        return updatedObject(state,{loading : false});
    }
    return state;
};

export default reducer;

