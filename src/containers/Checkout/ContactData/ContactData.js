import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import axios from '../../../axios';
import Button from '../../../components/UI/Button/Button';
import classes from './ContactData.css';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Input from '../../../components/UI/Input/Input';
import * as orderAction from '../../../store/actions/index'
import withErrorHandler from '../../../hoc/WithErrorHandler/WithErrorHandler';
import { updatedObject, validateELement }  from '../../../shared/utility';

class ContactData extends Component
{
    state = {
        orderForm : {
            name : {
                elementType : 'input',
                elementConfig : {
                    placeholder : 'Your Name',
                    type : 'text'
                },
                value : '',
                validation : {
                    required : true
                },
                valid : false,
                touched : false
            },
            streetName : {
                elementType : 'input',
                elementConfig : {
                    placeholder : 'Your Street Name',
                    type : 'text'
                },
                value : '',
                validation : {
                    required : true
                },
                valid : false,
                touched : false
            },
            zipcode : {
                elementType : 'input',
                elementConfig : {
                    placeholder : 'Your Zipcode',
                    type : 'text'
                },
                value : '',
                validation : {
                    required : true,
                    minLength : 5,
                    maxLength : 5
                },
                valid : false,
                touched : false
            },
            country : {
                elementType : 'input',
                elementConfig : {
                    placeholder : 'Your Country Name',
                    type : 'text'
                },
                value : '',
                validation : {
                    required : true
                },
                valid : false,
                touched : false
            },
            email : {
                elementType : 'input',
                elementConfig : {
                    placeholder : 'Your email',
                    type : 'email'
                },
                value : '',
                validation : {
                    required : true
                },
                valid : false,
                touched : false
            },
            delivery_method : {
                elementType : 'select',
                elementConfig : {
                    optionValue : [
                        {
                        value : 'home-delivery',
                        displayValue : 'Home Delivery'
                        },
                        {
                            value : 'self-pickup',
                            displayValue : 'Self Pickup'
                        }
                    ]
                },
                value : 'home-delivery',
                validation : {},
                valid : true
            },
        },
        isFormValid : false,
    }

    orderHandler = (event) => {
        event.preventDefault();

        const formDataValue = {};

        for (let key in this.state.orderForm) {
            formDataValue[key] = this.state.orderForm[key].value;
        }
        const order = {
            ingredients : this.props.ings,
            orderData : formDataValue,
            price : this.props.price,
            userId : localStorage.getItem('userId')
        };

        this.props.OnOrderStart(order, this.props.token);
      
    }

    

    inputChangedHandler = (event, inputIdentifire) => {
        

        // const updateOrderFormElement = {
        //     ...this.state.orderForm[inputIdentifire]
        // };

        const updateOrderFormElement = updatedObject(this.state.orderForm[inputIdentifire],{
            value : event.target.value,
            valid : validateELement(event.target.value, this.state.orderForm[inputIdentifire].validation),
            touched : true 
        });

        // const updatedOrderForm = {
        //     ...this.state.orderForm
        // };

        const updatedOrderForm = updatedObject(this.state.orderForm,{
            [inputIdentifire] : updateOrderFormElement
        });
         
        // updateOrderFormElement.value = ;
        // updateOrderFormElement.valid = this.validateELement(updateOrderFormElement.value, updateOrderFormElement.validation);
        // updateOrderFormElement.touched =- true;
        // updatedOrderForm[inputIdentifire] = updateOrderFormElement;
        let formIsValid = true;
        for (let inputIdentifire in updatedOrderForm) {
            formIsValid = updatedOrderForm[inputIdentifire].valid && formIsValid;
        }
        this.setState({
            orderForm : updatedOrderForm,
            isFormValid : formIsValid,
        })
    }
    render() {
        const formElementArray = [];

        for (let key in this.state.orderForm) {
            formElementArray.push({
                id : key,
                config : this.state.orderForm[key]
            })
        }
        let spin = (
            <form onSubmit={this.orderHandler}>
                {
                    formElementArray.map(form => (
                        <Input 
                            key={form.id}
                            elementType={form.config.elementType}
                            elementConfig={form.config.elementConfig}
                            value={form.config.value}
                            invalid={!form.config.valid}
                            shouldValidate={form.config.validation}
                            touched={form.config.touched}
                            changed={(event) => this.inputChangedHandler(event, form.id)}/>
                    ))
                }
            <Button clicked={this.orderHandler} disabled={!this.state.isFormValid} btnType="Success">Order</Button>
        </form>
        );
        if (this.props.loading) {
            spin = <Spinner/>;
        }

        let orderSuccess = null;
        if (this.state.orderSuccess) {
            orderSuccess = <h4>Your order has been successfully placed.</h4>
        }
        return (
            <div className={classes.ContactData}>
                <h4>Enter your contact data</h4>
                {orderSuccess}
                {spin}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ings : state.burgerBuilder.ingredients,
        price : state.burgerBuilder.totalPrice,
        loading : state.order.loading,
        token : state.auth.token
    }
}

const mapDispatchToProps = dispatch => {
    return {
        OnOrderStart : (order,token) => dispatch(orderAction.purchaseBurger(order,token)),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(withErrorHandler(ContactData, axios)));