import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import * as actionType from '../../store/actions/index';
import { connect }  from 'react-redux';
import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import Spinner from '../../components/UI/Spinner/Spinner'
import classes from './Auth.css';
import { validateELement }  from '../../shared/utility';

class Auth extends Component
{
    state = {
        controls : {
            email : {
                elementType : 'input',
                elementConfig : {
                    placeholder : 'Your email',
                    type : 'email'
                },
                value : '',
                validation : {
                    required : true
                },
                valid : false,
                touched : false
            },
            password : {
                elementType : 'input',
                elementConfig : {
                    placeholder : 'Your password',
                    type : 'password'
                },
                value : '',
                validation : {
                    required : true,
                    minLength : 5
                },
                valid : false,
                touched : false
            }
        },
        isFormValid : false,
        isSignUp : true
    }

    componentDidMount()
    {
        if (!(this.props.building) && (this.props.authRedirectPath !== "/")) {
            this.props.onAuthRedirect();   
        }
    }

    inputChangedHandler = (event, inputIdentifire) => {
        const updatedOrderForm = {
            ...this.state.controls
        };

        const updateOrderFormElement = {
            ...this.state.controls[inputIdentifire]
        };
         
        updateOrderFormElement.value = event.target.value;
        updateOrderFormElement.valid = validateELement(updateOrderFormElement.value, updateOrderFormElement.validation);
        updateOrderFormElement.touched =- true;
        updatedOrderForm[inputIdentifire] = updateOrderFormElement;
        let formIsValid = true;
        for (let inputIdentifire in updatedOrderForm) {
            formIsValid = updatedOrderForm[inputIdentifire].valid && formIsValid;
        }
        this.setState({
            controls : updatedOrderForm,
            isFormValid : formIsValid,
        })
    }

    submitHandler = (event) => {
        event.preventDefault();
        this.props.onAuthentication(this.state.controls.email.value, this.state.controls.password.value,this.state.isSignUp);
    }

    onAuthChangeHandler = () => {
        this.setState(prevState => {
            return {
                isSignUp : !prevState.isSignUp,
                isFormValid : false
            }
        })
    }
    render() {
        const formElementArray = [];

        for (let key in this.state.controls) {
            formElementArray.push({
                id : key,
                config : this.state.controls[key]
            })
        }

        let formData = formElementArray.map(form => (
                        <Input 
                            key={form.id}
                            elementType={form.config.elementType}
                            elementConfig={form.config.elementConfig}
                            value={form.config.value}
                            invalid={!form.config.valid}
                            shouldValidate={form.config.validation}
                            touched={form.config.touched}
                            changed={(event) => this.inputChangedHandler(event, form.id)}/>
                    ));
        let spinData = <Spinner/>;

        let errorMessages = null;
        if (this.props.error) {
            errorMessages = (
            <p style={{color : 'red'}}>{this.props.error.message}</p>
            );
        }

        let redirect = null;
        if (this.props.isAuthenticated) {
            redirect = <Redirect to={this.props.authRedirectPath}/>;
        }
        if (!this.props.loading) {
            spinData = (
                <div className={classes.Auth}>
                <form onSubmit={this.submitHandler}>
                    {redirect}
                    {errorMessages}
                {formData}
                <Button btnType="Success" disabled={!this.state.isFormValid}>SUBMIT</Button>
                </form>
                <Button btnType="Danger" clicked={this.onAuthChangeHandler}>SWITCH TO {this.state.isSignUp ? 'SIGNIN' : 'SIGNUP'}</Button>
            </div>
            );
        }
        return spinData;
    }
}

const mapStateToProps = state => {
    return {
        error : state.auth.error,
        loading : state.auth.loading,
        isAuthenticated : state.auth.token !== null,
        building : state.burgerBuilder.building,
        authRedirectPath : state.auth.authRedirect
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onAuthentication : (email, password, isSignUp) => dispatch(actionType.authentication(email, password, isSignUp)),
        onAuthRedirect : () => dispatch(actionType.authRedirect('/'))
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Auth);