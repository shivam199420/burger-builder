import React, { Component } from 'react';
import { connect } from 'react-redux';

import Aux from '../../hoc/Aux/aux'
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import axios from '../../axios';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/WithErrorHandler/WithErrorHandler';
import * as burgerBuilderAction from '../../store/actions/index'


class BurgerBuilder extends Component
{
    state = {
        error : false
    }

    componentDidMount() {
        this.props.initIngredients();
    }
    updatePurchasable = () => {
        let ingredients = this.props.ings;
        const sum = Object.keys(ingredients).map(igKey => {
            return ingredients[igKey];
        }).reduce((sum, el) => {
            return sum + el;
        },0);

       return sum > 0;
    }

    purcahseHandler = () => {
        if (this.props.isAutheticated) {
            this.setState({
                purchasing : true
            })    
        } else {
            this.props.onAuthRedirect('/checkout');
            this.props.history.push('/auth');
        }
        
    };

    purchaseCloseHandler = () => {
        this.setState({
            purchasing:false
        })
    };

    purchaseContinueHandler = () => {
        this.props.onInitPurchase();
        this.props.history.push('/checkout');
    }

    render() {
        const disabledInfo = {
            ...this.props.ings
        };
        for (let key in disabledInfo) {
            disabledInfo[key] = disabledInfo[key] <= 0;
        }

        let orderSummary = null;

        let burger = this.props.error ? <p>Something went wrong. Please try again</p> : <Spinner/>
        if (this.props.ings) {
            burger = (
                <Aux>
                    <Burger ingredients = {this.props.ings} />
                    <BuildControls
                    ingredientAdded = {this.props.onIngredientAdded}
                    ingredientLess = {this.props.onIngredientRemove}
                    disbaled = {disabledInfo}
                    totalPrice = {this.props.totalPrice}
                    purchasable = {this.updatePurchasable()}
                    ordered = {this.purcahseHandler}
                    isAuth = {this.props.isAutheticated}
                    />
                </Aux>
            );
            orderSummary = <OrderSummary 
                    ingredients = {this.props.ings}
                    modalClosed={this.purchaseCloseHandler}
                    continueClicked={this.purchaseContinueHandler}
                    totalPrice = {this.props.totalPrice}
                    />;
        }
        return (
            <Aux>
                <Modal 
                    show={this.state.purchasing}
                    modalClosed={this.purchaseCloseHandler}>
                    {orderSummary}
                </Modal>
                {burger}
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        ings : state.burgerBuilder.ingredients,
        totalPrice : state.burgerBuilder.totalPrice,
        error : state.burgerBuilder.error,
        isAutheticated : state.auth.token !== null,
    }

};

const mapDispatchToProps = dispatch => {
    return {
        onIngredientAdded : (ingName) => dispatch(burgerBuilderAction.addIngredient(ingName)),
        onIngredientRemove : (ingName) => dispatch(burgerBuilderAction.removeIngredient(ingName)),
        initIngredients : () => dispatch(burgerBuilderAction.initIngredients()),
        onInitPurchase : () => dispatch(burgerBuilderAction.purchaseInit()),
        onAuthRedirect : (path) => dispatch(burgerBuilderAction.authRedirect(path))
     } 
}

export default connect(mapStateToProps,mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios));