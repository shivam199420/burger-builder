import React, { Component } from 'react';
import Order from '../../components/Order/Order';
import axios from 'axios';
import ErrorHandler from '../../hoc/WithErrorHandler/WithErrorHandler';
import * as actions from '../../store/actions/index';
import { connect } from 'react-redux';
import Spinner from '../../components/UI/Spinner/Spinner';

class Orders extends Component {

    componentDidMount() {
        this.props.onFetchOrders(this.props.token,localStorage.getItem('userId'));
    }
    render() {
        let order = <Spinner/>
        if (!this.props.loading) {
            order = this.props.order.map(order => (
                        <Order key={order.id} ingredients = {order.ingredients} price = {order.price}/>
                    ));
        }
        return (
            <div>
                {order}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        order : state.order.orders,
        loading : state.order.loading,
        token : state.auth.token
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchOrders : (token,userId) =>  dispatch(actions.fetchOrders(token,userId))
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(ErrorHandler(Orders, axios));