import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import axios from 'axios';

import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import burgerBuilderReducer from './store/Reducers/burgerBuilder';
import orderReducer from './store/Reducers/order';
import authReducer from './store/Reducers/auth';

axios.defaults.baseURL = 'https://react-burger-4fcc1.firebaseio.com/';

const composeEnhancers = process.env.NODE_ENV === 'deveopment' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null || compose;

const rootReducers = combineReducers({
    order : orderReducer,
    burgerBuilder : burgerBuilderReducer,
    auth : authReducer
})

const store = createStore(rootReducers, composeEnhancers(applyMiddleware(thunk)));

const app = (
    <Provider store={store}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>
);

ReactDOM.render(app , document.getElementById('root'));
registerServiceWorker();
